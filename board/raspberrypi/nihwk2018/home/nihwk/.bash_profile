let upSeconds="$(/usr/bin/cut -d. -f1 /proc/uptime)"
let secs=$((${upSeconds}%60))
let mins=$((${upSeconds}/60%60))
let hours=$((${upSeconds}/3600%24))
let days=$((${upSeconds}/86400))
UPTIME=`printf "%d days, %02dh%02dm%02ds" "$days" "$hours" "$mins" "$secs"`

# get the load averages
read one five fifteen rest < /proc/loadavg

echo "$(tput setaf 2)


                                                                                                      
 _____    _____      ____________        __     __          _______     _______    ______   _______   
|\    \   \    \    /            \      /  \   /  \        /      /|   |\      \  |\     \  \      \  
 \\    \   |    |  |\___/\  \\___/|    /   /| |\   \      /      / |   | \      \  \\     \  |     /| 
  \\    \  |    |   \|____\  \___|/   /   //   \\   \    |      /  |___|  \      |  \|     |/     //  
   \|    \ |    |         |  |       /    \_____/    \   |      |  |   |  |      |   |     |_____//   
    |     \|    |    __  /   / __   /    /\_____/\    \  |       \ \   / /       |   |     |\     \   
   /     /\      \  /  \/   /_/  | /    //\_____/\\    \ |      |\\/   \//|      |  /     /|\|     |  
  /_____/ /______/||____________/|/____/ |       | \____\|\_____\|\_____/|/_____/| /_____/ |/_____/|  
 |      | |     | ||           | /|    | |       | |    || |     | |   | |     | ||     | / |    | |  
 |______|/|_____|/ |___________|/ |____|/         \|____| \|_____|\|___|/|_____|/ |_____|/  |____|/   
                                                                                                      

 Uptime.............: ${UPTIME}
 Memory.............: `cat /proc/meminfo | grep MemFree | awk {'print $2'}`kB (Free) / `cat /proc/meminfo | grep MemTotal | awk {'print $2'}`kB (Total)
 Load Averages......: ${one}, ${five}, ${fifteen} (1, 5, 15 min)
 Homepage...........: https://ctf.nihwk.net/
$(tput sgr0)"
