#!/bin/sh

set -u
set -e

# Add a console on tty1
if [ -e ${TARGET_DIR}/etc/inittab ]; then
    grep -qE '^tty1::' ${TARGET_DIR}/etc/inittab || \
	sed -i '/GENERIC_SERIAL/a\
tty1::respawn:/bin/login -f nihwk tty1 </dev/tty1 >/dev/tty1 2>&1' ${TARGET_DIR}/etc/inittab
fi
