################################################################################
#
# ARMSNES
#
################################################################################
ARMSNES_VERSION = armv7-compile. 
ARMSNES_SITE = git@github.com:RetroPie/ARMSNES-libretro.git
ARMSNES_SITE_METHOD = git
ARMSNES_TARGET = libarmsnes.so

define ARMSNES_BUILD_CMDS
	$(MAKE) CC="$(TARGET_CC)" CXX="$(TARGET_CXX)" \
		CFLAGS="$(TARGET_CFLAGS)" LD="$(TARGET_LD)" \
		TARGET="$(ARMSNES_TARGET)" -C $(@D) all
endef

define ARMSNES_INSTALL_TARGET_CMDS
	$(INSTALL) -D $(@D)/$(ARMSNES_TARGET) \
		$(TARGET_DIR)/usr/lib/libretro/$(ARMSNES_TARGET)
endef

$(eval $(generic-package))
